FROM maven:3.6.0-jdk-11-slim

MAINTAINER Christophe Maldivi

RUN export MONGODB_VERSION=3.6.11 \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -yq gnupg2 \
    && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5 \
    && echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/3.6 main" | tee /etc/apt/sources.list.d/mongodb-org-3.6.list \
    && apt-get update \
    && apt-get install -yq mongodb-org=$MONGODB_VERSION mongodb-org-server=$MONGODB_VERSION mongodb-org-shell=$MONGODB_VERSION mongodb-org-mongos=$MONGODB_VERSION mongodb-org-tools=$MONGODB_VERSION \
    && apt-get install -yq nginx net-tools \
    && apt-get install -yq curl wget coreutils git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*